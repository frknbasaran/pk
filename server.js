const chalk = require('chalk');

/*
	Task objects
*/
let personsFirst = [
    {
        id: 0,
        firstName: "Juerg",
        lastName: "Paulison",
        details: {
            age: 5
        }
    }, {
        id: 1,
        firstName: "Peter",
        lastName: "Mueller",
        details: {
            age: 5
        }
    }, {
        firstName: "Manfred",
        lastName: "Schneider",
        details: {
            age: 17
        }
    }, {
        id: 3,
        firstName: "Hans",
        lastName: "Mettmann",
        details: {
            age: 3
        }
    }, {
        firstName: "Dieter",
        lastName: "Schmitt"
    }, {
        firstName: "Klaudia",
        lastName: "Meier",
        details: {
            age: 23
        }
    }
];
let personsSecond = [
    {
        id: 0,
        firstName: "Juerg",
        lastName: "Paulison",
        details: {      age: 5}
    },
    {
        id: 1,
        firstName: "Peter",
        lastName: "Mueller",
        details: {      age: 5}
    },
    {
        id: 2,
        firstName: "Manfred",
        lastName: "Schneider",
        details: {age: 17}
    },
    {
        id: 3,
        firstName: "Hans",
        lastName: "Mettmann",
        details: {age: 3}
    },
    {
        id: 4,
        firstName: "Dieter",
        lastName: "Schmitt"
    },
    {
        id: 5,
        firstName: "Klaudia",
        lastName: "Meier",
        details: {age: 23}
    }
]
var personsThird = [
    {
        id: 0,
        firstName: "Juerg",
        lastName: "Paulison",
        details: {      age: 5}
    },
    {
        id: 1,
        firstName: "Peter",
        lastName: "Mueller",
        details: {      age: 5}
    },
    {
        id: 2,
        firstName: "Manfred",
        lastName: "Schneider",
        details: {age: 17}
    },
    {
        id: 3,
        firstName: "Hans",
        lastName: "Mettmann",
        details: {age: 3}
    },
    {
        id: 4,
        firstName: "Dieter",
        lastName: "Schmitt"
    },
    {
        id: 5,
        firstName: "Klaudia",
        lastName: "Meier",
        details: {age: 23}
    }
]
let person = {
    id: 1,
    firstName: "Peter",
    lastName: "Mueller",
    details: {
        age: 5
    },
    route: "rainbowstreet 15",
    postal_code: 55112,
    route2: "rainbowstreet 15",
    postal_code2: 55112,
    route3: "rainbowstreet 15",
    postal_code3: 55112,
    route4: "rainbowstreet 15",
    postal_code4: 55112,
    route5: "rainbowstreet 15",
    postal_code5: 55112
}

// Task1
let personsContaintsId = personsFirst.filter(p => p.hasOwnProperty("id"));
// Task2
// Task 3
let formattedPerson = { firstName: person.firstName, lastName: person.lastName, details: person.details };
let personsWithFullName = personsSecond.map(p => `${p.firstName} ${p.lastName}`);
// Task 4
let personsWhoOlderThan16 = personsThird.filter(p => p.details && p.details.age && p.details.age > 16);

console.log(chalk.bold.green("########## PIXELKULTUR JS CASE ##########"));

console.log(chalk.bold.red("########## Task 1 ##########"))
console.log(chalk.italic('Give a function which returns a list containing all elements who have an id.'));
console.log(chalk.yellow(JSON.stringify(personsContaintsId)));

console.log(chalk.bold.red("########## Task 2 ##########"))
console.log(chalk.italic('Give a function which returns a list containing all names in fullname-format'));
console.log(chalk.yellow(personsWithFullName));

console.log(chalk.bold.red("########## Task 3 ##########"))
console.log(chalk.italic('Give a function to return an object only containing "firstName", "lastName" and "details"'));
console.log(chalk.yellow(JSON.stringify(formattedPerson)));

console.log(chalk.bold.red("########## Task 4 ##########"))
console.log(chalk.italic('Give a function which returns all persons which are older than 16 (which runs without failures, e.g. missing data)'));
console.log(chalk.yellow(JSON.stringify(personsWhoOlderThan16)));

/*
	Object that contains customer and order details
	
	Order fields:
		- locality
	Customer Fields:
		- firstName
		- lastName
		- tnumber
		- mnumber
		- email
		- id
*/
const dataSentToServer = {
	locality: "Istanbul",
	id: 13,
	firstName: "Peter",
	lastName: "Mueller",
	tnumber: "+492151123456",
	mnumber: "+49151987654",
	email: "peter.mueller@gmail.com"
};

// customers array
const customers = [
	{
		customerId: 13,
		firstName: "Peter",
		lastName: "Mueller",
		tnumber: "+492151123456",
		mnumber: "+49151987654",
		email: "peter.mueller@gmail.com"
	},
	{
		customerId: 14,
		firstName: "Peter",
		lastName: "Mueller",
		tnumber: "+492151123456",
		mnumber: "+49151987654",
		email: "peter.mueller@gmail.com"
	}
];

// orders array
const orders = [ 
    {
		locality: "Istanbul",
		id: 1,
		customerId: 13,
	},
	{
		locality: "Istanbul",
		id: 2,
		customerId: 14,
	}
];

// parse request object to customer and order
const parseRequest = (data) => {
	return {
		customer: {
			id: data.id,
			firstName: data.firstName,
			lastName: data.lastName,
			tnumber: data.tnumber,
			mnumber: data.mnumber,
			email: data.email
		},
		order: {
			locality: data.locality,
			customerId: data.id
		}
	}
};

/*
* param: pro {Object} - parsed request object
*/
const checkDuplicate = (pro) => {
	let duplicates = [];

	// check is exist any order with the same locality
	orders.forEach((order) => {
		if (order.locality === pro.order.locality) {
			// check at least one field of [lastName, tnumber, mnumber, email] is same
			customers.forEach((customer) => {
				if (customer.customerId === order.customerId && (customer.lastName === pro.customer.lastName || customer.tnumber === pro.customer.tnumber || customer.mnumber === pro.customer.mnumber || customer.email === pro.customer.email)) {
					duplicates.push({
						locality: order.locality, 
						id: order.id, 
						customerId: customer.customerId, 
						firstName:customer.firstName, 
						lastName: customer.lastName, 
						tnumber:customer.tnumber, 
						email:customer.email
					});
				} 
			});		
		} 
	});

	return (duplicates.length > 0) ? duplicates : false;
}

if (!checkDuplicate(parseRequest(dataSentToServer))) {
	customers.push(parseRequest(dataSentToServer).customer);
	orders.push(parseRequest(dataSentToServer).order);
} else {
	console.log(chalk.bold.red("########## Task 5 ##########"));
	console.log(chalk.yellow(JSON.stringify(checkDuplicate(parseRequest(dataSentToServer)))));
}

